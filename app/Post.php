<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage; 
use App\Category;
use App\Tag ;


class Post extends Model
{   
    use SoftDeletes;
    protected $fillable =[
        'title',
        'description',
        'content',
        'image',
        'published_at',
        'category_id'
    ];
    /**
     * Delete post
     * @return void 
     */

    public function deleteImage(){
        Storage::delete($this->image);
    }
    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function tags(){
        return $this->belongsToMany(Tag::class);
    }
    /**
     * mengembalikan id tag yang dimiliki oleh post. Hasil pemrosesan saat create adalah
     * array dalam array jadi arraynya itu cuman diambil idnya(casenya elemen arraynya banyak)
     * 
     * @return bool
     */
    public function hasTag($TagId){
        return in_array($TagId, $this->tags->pluck('id')->toArray());
    }
}
